
var $passwordFeilds = $(".password-wrap");

function makeShowPass(el) {
    el.each( function() {

        var $showPassBtn = $(this).find("button[name='show-pass']");
        var $passInput = $(this).find("input[type='password']");

        $showPassBtn.on('click', function(){
            if ($passInput.hasClass('is-show')) {
                $passInput.attr('type', 'password');
                $passInput.removeClass('is-show');

            } else {
                $passInput.attr('type', 'text');
                $passInput.addClass('is-show');
            }
        });


    });
};
makeShowPass($passwordFeilds);
