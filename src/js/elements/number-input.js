
var $steppers = $(".stepper-wrap, .range-selector-wrap");


function makeSteppers(el) {

    el.each( function(){
        var $stepUp = $(this).find("button[name='step-up']");
        var $stepDown = $(this).find("button[name='step-down']");
        var $input = $(this).find("input[type='number']")
        var min = $input.attr('min');
        var max = $input.attr('max');
        var step = parseFloat($input.attr('step'));

        $stepUp.on('click', function(){
            handelStep('up');
        });
        $stepDown.on('click', function(){
            handelStep('down');
        });

        function handelStep(direction) {
            var oldVal = parseFloat($input.val());

            if (oldVal < min) {
                $input.val(min);

            } else if (oldVal > max) {
                $input.val(max);

            } else if (direction == 'up' && oldVal < max) {
                var newVal = oldVal + step;
                $input.val(newVal);

            } else if (direction == 'down' && oldVal > min) {
                var newVal = oldVal - step;
                $input.val(newVal);
            }

        };
    });

};
makeSteppers($steppers);
