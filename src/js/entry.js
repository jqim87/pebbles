$(document).foundation();
require('./elements/number-input.js');
require('./elements/text-input.js');
require('./elements/date-picker.js');
// require('./components/show-hide.js');
var showHide = require('./components/show-hide');
var stickyTotal = require('./components/sticky-total.js');

$(document).ready(function(){

    showHide.initLinked($('.show-hide-wrap-linked'), 'large');
    showHide.initSeparate($('.show-hide-wrap'));
    stickyTotal.init($('#stickyTotal'), $('.topnav'));

});
