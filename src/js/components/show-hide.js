var clickHandler = null;
var clickHandlerLinked = null;
var size = null;
var $allControls = null;
var $allPanels = null;

function calculateClick() {

    if (Foundation.MediaQuery.atLeast(size)) {
        if ($allPanels.hasClass('is-open')) {
            $allControls.addClass('is-open');
            $allPanels.addClass('is-open');
        }
        clickHandlerLinked = function(){
            $allControls.toggleClass('is-open');
            $allPanels.toggleClass('is-open');
        };

    } else {
        clickHandlerLinked = function($thisControl, $thisPanel){
            $thisControl.toggleClass('is-open');
            $thisPanel.toggleClass('is-open');
        };
    }

}

function initLinked($el, setSize) {

    if (setSize !== undefined) {
        size = setSize;
        $allControls = $el.find('.show-hide-control');
        $allPanels = $el.find('.show-hide-panel');
    } else {
        console.error('no breakpoint specified for initLinked()');
    }

    $el.each(function(){
        var $control = $(this).find('.show-hide-control');
        var $panel = $(this).find('.show-hide-panel');

        $control.on('click', function(){
            clickHandlerLinked($control, $panel);

        });

    });

    calculateClick();

    //https://css-tricks.com/snippets/jquery/done-resizing-event/
    var resizeTimer;
    $(window).on('resize', function(e) {

      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {

        // Run code here, resizing has "stopped"
        calculateClick()

      }, 250);

    });


};

function initSeparate($el) {

    clickHandler = function($thisControl, $thisPanel){
        $thisControl.toggleClass('is-open');
        $thisPanel.toggleClass('is-open');
    };

    $el.each(function(){
        var $control = $(this).find('.show-hide-control');
        var $panel = $(this).find('.show-hide-panel');

        $control.on('click', function(){
            clickHandler($control, $panel);

        });

    });

};
// initLinked($('.show-hide-wrap-linked'), 'large');
// initSeparate($('.show-hide-wrap'));
exports.initSeparate = initSeparate;
exports.initLinked = initLinked;
