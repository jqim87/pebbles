function init($el, $clearOf){

    var topVal = $clearOf.outerHeight();

    function makeSticky(){
        if (Foundation.MediaQuery.is('large')) {
            $el.$sticky = new Foundation.Sticky( $el, {
                stickyOn: 'medium',
                stickTo: 'bottom',
                marginBottom:'0',
            } );

        } else {
            $el.$sticky = new Foundation.Sticky( $el, {
                stickyOn: 'small',
                topAnchor: topVal,
                marginTop:'0'
            } );
        }
    };
    makeSticky();


    var resizeTimer;
    $(window).on('resize', function(e) {

      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {

        // Run code here, resizing has "stopped"
        $el.foundation('_destroy');
        makeSticky();

    }, 500);

    });


};

//https://css-tricks.com/snippets/jquery/done-resizing-event/


exports.init = init;
