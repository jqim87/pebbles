/* eslint-disable */
const gulp = require( 'gulp' );

const sourcemaps = require( 'gulp-sourcemaps' );
const watch = require( 'gulp-watch' );

const browserify = require('browserify');
const babelify = require('babelify');
const uglify = require('uglifyify');

const sass = require( 'gulp-sass' );
const autoprefixer = require( 'gulp-autoprefixer' );
const njRender = require( 'gulp-nunjucks-render' );
const imagemin = require('gulp-imagemin');

const browserSync = require( 'browser-sync' ).create();
const log = require( 'fancy-log' );
const data = require( 'gulp-data' );
const del = require('del');
const source = require('vinyl-source-stream')
// Source Directories
const srcBaseDir = 'src';

const srcPaths = {
  scss: [`${srcBaseDir}/scss/**/*.scss`, `${srcBaseDir}/scss/*.scss`],
  js: [ `${srcBaseDir}/js/**/*.js`, `${srcBaseDir}/js/*.js` ],
  templates: [`${srcBaseDir}/templates/**/*.njk`],
  fonts: [`${srcBaseDir}/fonts/**/*`,
  './node_modules/material-design-icons/iconfont/*.+(eot|ttf|woff|woff2|)'],
  images: [`${srcBaseDir}/images/**/*`],
  assets: [`${srcBaseDir}/assets/**/*`]
};
const sassDirs = './node_modules/foundation-sites/scss';



// Destination directories wokring files
const destBaseDir = 'dev';

const destPaths = {
  scss: `${destBaseDir}/`,
  js: `${destBaseDir}/js`,
  templates: `${destBaseDir}`,
  fonts: `${destBaseDir}/fonts`,
  images: `${destBaseDir}/images`,
  assets: `${destBaseDir}/assets`,
  htmlBlocks:`${destBaseDir}/html-blocks`
};

// Destination directories for final builds
const publishBaseDir = 'dist';

const publishPaths = {
  scss: `${publishBaseDir}/`,
  js: `${publishBaseDir}/js`,
  templates: `${publishBaseDir}`,
  fonts: `${publishBaseDir}/fonts`,
  images: `${publishBaseDir}/images`,
  assets: `${publishBaseDir}/assets`,
  htmlBlocks:`${publishBaseDir}/html-blocks`
};


gulp.task( 'serve', ['scss', 'js', 'templates', 'fonts', 'images', 'assets'], () => {
  browserSync.init( {
    open: false,
    server: {
      baseDir: './dev',
      serveStaticOptions: {
        extensions: ['html']
      }
    },
    notify: {
      styles: {
        top: 'auto',
        bottom: '20px',
        right: '20px',
        backgroundColor: '#fff',
        color: '#000',
        borderRadius: 0,
        boxShadow: '1px 1px 8px 0px rgba( 0, 0, 0, 0.4 )',
      },
    },
  } );

  // Start all watch tasks
  gulp.start( 'scss:watch' );
  gulp.start( 'js:watch' );
  gulp.start( 'templates:watch' );
  gulp.start( 'fonts:watch' );
  gulp.start( 'images:watch' );
} );

// TEMPLATES
gulp.task( 'templates:watch', ['templates'], () => {
  watch( srcPaths.templates, () => {
    return gulp.src( 'src/templates/**/!(_)*.njk' )
    // .pipe( data( ( file ) => {
      // return require( `./src/data/data-local.json` )
    // } ) )
    .pipe(
      njRender( {
        path: ['src/templates'],
      } )
    )
    .on( 'error', function ( err ) {
      log( '[Compilation Error]' );
      log(
        err.fileName +
          ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
      );
      log( `error Nunjucks: ${err.message}\n` );
      log( err.codeFrame );
      this.emit( 'end' );
    })
    .pipe( gulp.dest( destPaths.templates ) )
    .pipe( browserSync.stream() )
  } )
})

gulp.task( 'templates', () => {
    return gulp.src( 'src/templates/**/!(_)*.njk' )
    // .pipe( data( ( file ) => {
      // return require( `./src/data/data-local.json` )
    // } ) )
    .pipe(
      njRender( {
        path: ['src/templates'],
      } )
    )
    .on( 'error', function ( err ) {
      log( '[Compilation Error]' );
      log(
        err.fileName +
          ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
      );
      log( `error Nunjucks: ${err.message}\n` );
      log( err.codeFrame );
      this.emit( 'end' );
    })
    .pipe( gulp.dest( destPaths.templates ) )
} );

gulp.task( 'blocks', () => {
    return gulp.src( 'src/templates/**/@(_)*.njk' )
    // .pipe( data( ( file ) => {
      // return require( `./src/data/data-local.json` )
    // } ) )
    .pipe(
      njRender( {
        path: ['src/templates'],
      } )
    )
    .on( 'error', function ( err ) {
      log( '[Compilation Error]' );
      log(
        err.fileName +
          ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
      );
      log( `error Nunjucks: ${err.message}\n` );
      log( err.codeFrame );
      this.emit( 'end' );
    })
    .pipe( gulp.dest( destPaths.htmlBlocks ) )
} );
gulp.task( 'blocks:build', () => {
    return gulp.src( 'src/templates/**/@(_)*.njk' )
    // .pipe( data( ( file ) => {
      // return require( `./src/data/data-local.json` )
    // } ) )
    .pipe(
      njRender( {
        path: ['src/templates'],
      } )
    )
    .on( 'error', function ( err ) {
      log( '[Compilation Error]' );
      log(
        err.fileName +
          ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
      );
      log( `error Nunjucks: ${err.message}\n` );
      log( err.codeFrame );
      this.emit( 'end' );
    })
    .pipe( gulp.dest( publishPaths.htmlBlocks ) )
} );

// SCSS

gulp.task( 'scss', () => {
  return gulp.src( srcPaths.scss )
  .pipe( sourcemaps.init() )
  .pipe( sass( {
    includePaths: sassDirs,
  } ).on( 'error', sass.logError ) )
  .pipe( autoprefixer({
    browsers: ['last 4 versions']
  }))
  .pipe( sourcemaps.write() )
  .pipe( gulp.dest( destPaths.scss ) )
  .pipe( browserSync.stream() );
})

gulp.task( 'scss:watch', () => {
  return watch( srcPaths.scss, () => {
    // log( `Running '${colors.cyan( 'scss' )}'` );

    gulp.src( srcPaths.scss )
    .pipe( sourcemaps.init() )
    .pipe( sass( {
      includePaths: sassDirs,
    } ).on( 'error', sass.logError ) )
    .pipe( autoprefixer({
      browsers: ['last 4 versions']
    }))
    .pipe( sourcemaps.write() )
    .pipe( gulp.dest( destPaths.scss ) )
    .pipe( browserSync.stream() );
  })
});


// JAVASCRIPT
const bundleOpts = {
  transform: [
    require('babelify').configure({
      // presets: [ 'es2015' ]
    })
  ]
}

gulp.task( 'js', () => {
  return browserify(bundleOpts)
  .add('./src/js/entry.js' )
  .bundle()
  .on( 'error', function ( err ) {
      log( '[Compilation Error]' );
      log(
        err.fileName +
          ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
      );
      log( `error Babel: ${err.message}\n` );
      log( err.codeFrame );
      this.emit( 'end' );
    } )
    .pipe(source('main.js'))
    .pipe( gulp.dest( destPaths.js ) )
    .pipe( browserSync.stream() )
})

gulp.task( 'js:watch', () => {

  return watch( srcPaths.js, () => {

    browserify(bundleOpts)
    .add('./src/js/entry.js' )
    .bundle()
    .on( 'error', function ( err ) {
        log( '[Compilation Error]' );
        log(
          err.fileName +
            ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
        );
        log( `error Babel: ${err.message}\n` );
        log( err.codeFrame );
        this.emit( 'end' );
      } )
      .pipe(source('main.js'))
      .pipe( gulp.dest( destPaths.js ) )
      .pipe( browserSync.stream() )
    } )
});

// FONTS
gulp.task( 'fonts', () => {
  return gulp.src( srcPaths.fonts )
  .pipe( gulp.dest( destPaths.fonts ) )
})

gulp.task( 'fonts:watch', () =>
  watch( srcPaths.fonts )
  .pipe( gulp.dest( destPaths.fonts ) )
);


gulp.task( 'assets', () => {
  return gulp.src( srcPaths.assets )
  .pipe( gulp.dest( destPaths.assets ))
})

gulp.task( 'assets:build', () => {
  return gulp.src( srcPaths.assets )
  .pipe( gulp.dest( publishPaths.assets ))
})

// IMAGES
gulp.task( 'images', () => {
  return gulp.src( srcPaths.images )
  // .pipe( imagemin( [
  //   imagemin.jpegtran( { interlaced: true } ),
  //   imagemin.optipng( { optimizationLevel: 5 } ),
  //   imagemin.svgo()
  // ], {
  //   verbose: true
  // } ) )
  .pipe( gulp.dest( destPaths.images ) )
})

gulp.task( 'images:watch', () => {
  watch( srcPaths.images )
  .pipe( imagemin( [
    imagemin.jpegtran( { interlaced: true } ),
    imagemin.optipng( { optimizationLevel: 5 } ),
    imagemin.svgo()
  ], {
    verbose: true
  } ) )
  .pipe( gulp.dest( destPaths.images ) )
});


// Build tasks

gulp.task( 'scss:build', () => {
  return gulp.src( srcPaths.scss)
    .pipe( sass( {
      includePaths: sassDirs,
      outputStyle: 'compressed',
    } ).on( 'error', sass.logError ) )
    .pipe( autoprefixer({
      browsers: ['last 4 versions']
    }))
    .pipe( gulp.dest( publishPaths.scss ) )
})

gulp.task( 'js:build', () => {
  return browserify(bundleOpts)
    .add('./src/js/entry.js')
    .transform(uglify, { global: true })
    .bundle()
    .on( 'error', function ( err ) {
        log( '[Compilation Error]' );
        log(
          err.fileName +
            ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
        );
        log( `error Babel: ${err.message}\n` );
        log( err.codeFrame );
        this.emit( 'end' );
      } )
      .pipe(source('main.js'))
      .pipe( gulp.dest( publishPaths.js ) )
})

gulp.task( 'templates:build', () => {
  return gulp.src( 'src/templates/**/!(_)*.njk' )
    // .pipe( data( ( file ) => {
    //   return require( `./src/data/data.json` )
    // } ) )
    .pipe(
      njRender( {
        path: ['src/templates'],
      } )
    )
    .on( 'error', function ( err ) {
      log( '[Compilation Error]' );
      log(
        err.fileName +
          ( err.loc ? `( ${err.loc.line}, ${err.loc.column} ): ` : ': ' )
      );
      log( `error Nunjucks: ${err.message}\n` );
      log( err.codeFrame );
      this.emit( 'end' );
    })
    .pipe( gulp.dest( publishPaths.templates ) )
})

gulp.task( 'fonts:build', () =>
  gulp
  .src( srcPaths.fonts )
  .pipe( gulp.dest( publishPaths.fonts ) )
);

gulp.task( 'images:build', () => {
  gulp.src( srcPaths.images )
  .pipe( imagemin( [
    imagemin.jpegtran( { interlaced: true } ),
    imagemin.optipng( { optimizationLevel: 5 } ),
    imagemin.svgo()
  ], {
    verbose: true
  } ) )
  .pipe( gulp.dest( publishPaths.images ) )
});


gulp.task( 'clean:all', () => {
  return del.sync([
    './dist'
  ]);
})

gulp.task( 'build', ['fonts:build', 'images:build', 'templates:build', 'scss:build', 'js:build', 'assets:build', 'blocks:build'], () => {

} )
