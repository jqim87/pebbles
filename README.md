# pebbles
##Installation
Clone the project
```bash
$npm install
$npm run gulp serve
```
This will fill the 'dev' folder with assets and code and then run a local server

##Publishing
```bash
$npm run gulp build
```
This will fill the 'dist' folder with optimized assets and code

```bash
$npm run gulp blocks
```
This will fill the dist folder with HTML snippets to upload to Frontify

### CSS
General rules:
1. Use the Foundation framework before writing more CSS
2. Class style and structure is ITCSS, which follows the Foundation framework style
3. Idiomatic CSS for commending style
4. All values input with named variables
5. Any 'magic numbers' must be explained in comments
